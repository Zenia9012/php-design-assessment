<?php

require_once 'app/Request.php';

use App\Request;

/**
 * Send assessment to www.coredna.com
 */
function sendAssessment()
{
    $tokenResponse = Request::options('https://www.coredna.com/assessment-endpoint.php');

    return Request::post(
        'https://www.coredna.com/assessment-endpoint.php',
        [
            'name' => 'Yevhenii Riabyi',
            'email' => 'zenia9012@gmail.com',
            'url' => 'https://bitbucket.org/Zenia9012/php-design-assessment/src/master/',
        ],
        [
            'Authorization' => 'Bearer ' . $tokenResponse->getBody(),
            'content-type' => 'application/json',
        ]
    );
}

try {
    var_dump(sendAssessment());
} catch (Exception $e) {
    echo $e->getMessage();
}
