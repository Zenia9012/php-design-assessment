<?php


/**
 * Class Response
 */
class Response {

    private $response;
    private $headers;

    /**
     * Response constructor.
     *
     * @param       $response
     * @param array $headers
     */
    public function __construct($response, $headers = [])
    {
        $this->response = $response;
        $this->headers = $headers;
    }

    /**
     * Returns body response in correct (string / JSON array) form.
     *
     * @return mixed[]/string
     * @throws Exception On json_decode errors
     */
    public function getBody()
    {
        // If the payload is in json, try to decode json.
        if (strpos(strtolower(implode(', ', $this->getHeaders())), 'application/json') !== false) {
            return json_decode($this->response, true, $depth = 512, JSON_THROW_ON_ERROR);
        }

        return $this->response;
    }

    /**
     * Returns response header.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }
}
