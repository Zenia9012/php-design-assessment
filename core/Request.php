<?php

namespace Core;

use Response;
use Exception;

require_once 'core/Response.php';

class Request {

    private $url;
    private $method;
    private $body;
    private $headers;

    /**
     * Request constructor.
     *
     * @param $url
     * @param $method
     * @param $body
     * @param $headers
     */
    public function __construct($url, $method, $body, $headers)
    {
        $this->url = $url;
        $this->method = strtoupper($method);
        $this->body = $body;
        $this->headers = array_change_key_case($headers, CASE_LOWER);

        if (empty($headers['content-type'])) {
            $headers['content-type'] = 'application/x-www-form-urlencoded';
        }
    }

    /**
     * Build structure for Request
     *
     * @throws Exception
     */
    private function buildStructure()
    {
        switch ($this->method) {
            case 'OPTIONS':
            case 'GET':
                if (is_array($this->body)) {
                    if (strpos($this->url, '?') == false) {
                        $this->url .= '?';
                    } else {
                        $this->url .= '&';
                    }

                    $this->url .= urldecode(http_build_query($this->body));
                }
                break;
            case 'DELETE':
            case 'PUT':
            case 'POST':
                if (is_array($this->body)) {
                    switch ($this->headers['content-type']) {
                        case 'application/x-www-form-urlencoded':
                            $this->body = http_build_query($this->body);
                            break;
                        case 'application/json':
                            $this->body = json_encode($this->body);
                            break;
                    }
                }
                break;
            default:
                throw new Exception('Unexpected method ' . $this->method);
        }

        $options = [
            'http' => [
                'method' => $this->method,
            ],
        ];

        if ($this->headers) {
            $options['http']['header'] = implode(
                "\r\n",
                array_map(
                    function ($v, $k) {
                        return sprintf("%s: %s", $k, $v);
                    },
                    $this->headers,
                    array_keys($this->headers)
                )
            );
        }

        if ($this->body) {
            $options['http']['content'] = $this->body;
        }

        return $options;
    }

    /**
     * Sends request
     *
     * @return Response
     * @throws Exception
     */
    public function send()
    {
        $options = $this->buildStructure();

        $context = stream_context_create($options);
        $result = file_get_contents($this->url, false, $context);

        if ($result === false) {
            $status_line = implode(',', $http_response_header);
            preg_match('{HTTP\/\S*\s(\d{3})}', $status_line, $match);
            $status = $match[1];

            if (strpos($status, '2') !== 0 && strpos($status, '3') !== 0) {
                throw new Exception("Unexpected status: {$status}");
            }
        }

        return new Response($result, $http_response_header);
    }
}
