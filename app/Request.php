<?php

namespace App;

require_once 'core/Request.php';
require_once 'app/Request.php';


use Exception;
use Core\Request as CoreRequest;

/**
 * Class Request
 */
class Request {

    /**
     * get request
     *
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return mixed
     * @throws Exception
     */
    public static function get($url, $body = null, $headers = []): CoreRequest
    {
        return (new CoreRequest($url, 'GET', $body, $headers))->send();
    }

    /**
     * post request
     *
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return mixed
     * @throws Exception
     */
    public static function post($url, $body = null, $headers = [])
    {
        return (new CoreRequest($url, 'POST', $body, $headers))->send();
    }

    /**
     * put request
     *
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return mixed
     * @throws Exception
     */
    public static function put($url, $body = null, $headers = [])
    {
        return new CoreRequest($url, 'PUT', $body, $headers);

    }

    /**
     * delete request
     *
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return mixed
     * @throws Exception
     */
    public static function delete($url, $body = null, $headers = [])
    {
        return new CoreRequest($url, 'DELETE', $body, $headers);
    }


    /**
     * options request
     *
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return mixed
     * @throws Exception
     */
    public static function options($url, $body = null, $headers = [])
    {
        return (new CoreRequest($url, 'OPTIONS', $body, $headers))->send();
    }
}
